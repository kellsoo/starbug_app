package com.example.root.starbug_app;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import java.util.Random;

public class MainActivity extends AppCompatActivity {



    Random a_rand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        a_rand = new Random(System.currentTimeMillis());
    }


    /// On click method for button 1
    public void butt_1_on_click(View v) {

        /// creating and start new Intent for starting new screen - activity_hello_world_.xml
        Intent intent = new Intent(MainActivity.this, hello_world.class);
        startActivity(intent);
    }


    /// On click method for button 2
    public void butt_2_on_click(View v) {
        show_message_box("Test alert", "Alert", "Ok");
    }


    /// On click method for button 3
    public void butt_3_on_click(View v) {

        /// creating and start new Intent for starting new screen - list_of_records.xml
        Intent intent = new Intent(MainActivity.this, list_of_records.class);
        startActivity(intent);
    }


    /// On click method for button 4

    public void butt_4_on_click(View v) {

        ///finding Lienar layout form activity_main.xml
        LinearLayout my_lin_lay = (LinearLayout)findViewById(R.id.main_lin_lay);

        /// changing background color through method get_new_color
        my_lin_lay.setBackgroundColor(get_new_color());
    }



    ///method for creating alert (mesagebox - name from C#)
    private void show_message_box(String pa_message_text, String pa_title, String pa_button_text) {
        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
        dlgAlert.setMessage(pa_message_text);
        dlgAlert.setTitle(pa_title);
        dlgAlert.setPositiveButton(pa_button_text, null);
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }



    ///method for random generating colors in int form
    private int get_new_color()
    {
        return Color.argb(255, a_rand.nextInt(256), a_rand.nextInt(256), a_rand.nextInt(256));
    }
}