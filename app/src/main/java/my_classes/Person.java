package my_classes;

import com.example.root.starbug_app.R;

public class Person {

    private String a_name;
    private int a_id;

    public Person(String pa_name, int pa_id) {
        this.a_name = pa_name;
        this.a_id = pa_id;
    }

    public String get_name() {
        return a_name;
    }

    public int get_id() {
        return a_id;
    }


    ///geter which get the id in string form
    public String get_id_as_str(){
        return ""+a_id;
    }

    public void set_name(String a_name) {
        this.a_name = a_name;
    }

    public void set_id(int a_id) {
        this.a_id = a_id;
    }


    @Override
    public String toString() {

        StringBuilder my_str_bulider = new StringBuilder();
        my_str_bulider.append(R.string.pers_name).append(": ").append(a_name).append("\n");
        my_str_bulider.append(R.string.pers_id).append(": ").append(a_id).append("\n");
        return my_str_bulider.toString();
    }
}
