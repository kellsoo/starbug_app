package com.example.root.starbug_app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Random;

import my_classes.Person;
import my_classes.PersonAdapter;

public class list_of_records extends AppCompatActivity {

    ListView a_simple_list;
    ArrayList<Person> a_my_list;
    Random a_rand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_records);

        a_rand = new Random(System.currentTimeMillis());

        a_my_list = new ArrayList<Person>();
        fill_data(3);
        a_simple_list = (ListView) findViewById(R.id.simpleList);

        PersonAdapter arrayAdapter = new PersonAdapter(this, R.layout.activity_list_view, a_my_list);
        a_simple_list.setAdapter(arrayAdapter);

        //add back button
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == android.R.id.home){
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }


    /// method for random generating persons
    private void fill_data(int pa_count){

        String list_of_name[] = {"Peter","Martin","Jozo","Anna","Jan","Tatiana","Erika","Veronika"};

        for (int i = 0 ; i < pa_count; i++){


            int name_index = a_rand.nextInt(list_of_name.length) ;
            
            a_my_list.add(new Person(list_of_name[name_index],name_index));
        }
    }
}
