package my_classes;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.root.starbug_app.R;

import java.util.ArrayList;

public class PersonAdapter extends ArrayAdapter<Person>{

    private Context a_context;
    private int a_lay_id;
    private ArrayList<Person> a_data;

    public PersonAdapter(@NonNull Context pa_context, int pa_resource, ArrayList<Person> pa_data) {
        super(pa_context, pa_resource,pa_data);
        a_context = pa_context;
        a_lay_id = pa_resource;
        a_data = pa_data;
    }


    /// seting of current row in list of persons (class), holder is wrapper for 2 textviews form activity list_view.xml, where are
    /// set the attributes from class person
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View row = convertView;
        Holder my_holder = null;


        if(row == null)
        {
            LayoutInflater inflater = ((Activity)a_context).getLayoutInflater();
            row = inflater.inflate(a_lay_id, parent, false);

            my_holder = new Holder();

            my_holder.txt_1 = (TextView)row.findViewById(R.id.txt_1);
            my_holder.txt_2 = (TextView)row.findViewById(R.id.txt_2);

            row.setTag(my_holder);
        }
        else
        {
            my_holder = (Holder)row.getTag();
        }

        Person actual = a_data.get(position);
        my_holder.txt_1.setText(actual.get_name());
        my_holder.txt_2.setText(actual.get_id_as_str());

        return row;
    }



    static class Holder {
        TextView txt_1;
        TextView txt_2;
    }
}
